/*
 * =====================================================================================
 *
 *       Filename:  CLZTest.cpp
 *
 *    Description:
 *
 *        Version:  1.0
 *        Created:  01/07/2017 06:27:43 PM
 *       Revision:  none
 *       Compiler:  gcc
 *
 *         Author:  YOUR NAME (),
 *   Organization:
 *
 * =====================================================================================
 */

#include "BitUtils.hpp"
#include "gtest/gtest.h"

#include <array>
#include <cstdint>
#include <limits>
#include <utility>
using namespace BitTables;

using Param = std::pair<std::int32_t, std::size_t>;

struct CLZTest : public ::testing::TestWithParam<Param> {};

TEST_P(CLZTest, testLeadingZeroesInt32) {
  ASSERT_EQ(std::get<1>(GetParam()),
            countLeadingZeroes(std::get<0>(GetParam())));
}

// clang-format off
INSTANTIATE_TEST_CASE_P(Int32Test, CLZTest, ::testing::Values(
      Param(0, 32), 
      Param(-1, 0), 
      Param(7, 29), 
      Param(255, 24),
      Param(INT32_MAX, 1), 
      Param(INT32_MIN, 0),
      Param(0x0000FFFF, 16), 
      Param(0x0FFFFFFF, 4),
      Param(0x01010101, 7),
      Param(0b0010'0000'0000'0000'0000'0000'0000'0000, 2),
      Param(0b0000'0000'0100'0000'0000'0000'0000'0000, 9),
      Param(0b0000'0000'0000'0000'0000'0001'0000'0000, 23),
      Param(0b0000'0000'0000'0000'0100'1000'1111'1111, 17),
      Param(0b1000'0000'0000'0000'0000'0000'0000'0000, 0)));
// clang-format on

template <typename T> struct CLZTypeTest : public ::testing::Test {};

using IntegralTypes =
    ::testing::Types<std::int8_t, std::uint8_t, std::int16_t, std::uint16_t,
                     std::int32_t, std::uint32_t, std::int64_t, std::uint64_t>;

TYPED_TEST_CASE(CLZTypeTest, IntegralTypes);

TYPED_TEST(CLZTypeTest, TypedSanityTest) {
  if (std::is_signed<TypeParam>::value) {
    ASSERT_EQ(1u, countLeadingZeroes(std::numeric_limits<TypeParam>::max()));
  } else {
    ASSERT_EQ(0u, countLeadingZeroes(std::numeric_limits<TypeParam>::max()));
  }

  const TypeParam Min = std::numeric_limits<TypeParam>::min();
  if (std::is_signed<TypeParam>::value) {
    ASSERT_EQ(0u, countLeadingZeroes(Min));
  } else {
    // All 0s
    ASSERT_EQ(sizeof(Min) * 8, countLeadingZeroes(Min));
  }

  const TypeParam Mid = TypeParam(1) << (sizeof(Min) * 4);
  ASSERT_EQ(sizeof(Mid) * 4 - 1, countLeadingZeroes(Mid));
}
