/*
 * =====================================================================================
 *
 *       Filename:  HllTest.cpp
 *
 *    Description:
 *
 *        Version:  1.0
 *        Created:  01/15/2017 06:49:22 PM
 *       Revision:  none
 *       Compiler:  gcc
 *
 *         Author:  YOUR NAME (),
 *   Organization:
 *
 * =====================================================================================
 */

#include "HLL.hpp"
#include <boost/random/discrete_distribution.hpp>
#include <boost/random/mersenne_twister.hpp>
#include <gtest/gtest.h>

#include <cmath>
#include <limits>
#include <unordered_set>
#include <vector>

#include <iostream>

using namespace boost::random;

namespace {
// Since we're using an int distribution and std::hash just forwards the input
// as std::size_t (commonly twice the sizeof int), we lose the approximation
// of a random distribution unless we cast the result back to the original
// integral type
template <typename T> struct SourceTypedHash {
  T operator()(const T Value) const noexcept {
    return static_cast<T>(H(Value));
  }
  std::hash<T> H;
};

inline void compareStdErr(const double ExpectedErr,
                          const std::size_t ActualSize,
                          const std::size_t EstimatedSize) {
  std::cout << "Expected: " << ExpectedErr << ", Actual: " << ActualSize
            << ", Estimate: " << EstimatedSize << std::endl;
  double ComputedErr =
      std::abs(static_cast<double>(EstimatedSize) / ActualSize - 1.0);
  ASSERT_LT(ComputedErr, ExpectedErr);
}

using SourceAccumulator = Accumulator<int, SourceTypedHash<int>>;
} // end anonymous namespace

TEST(HLLTest, flatHashShouldResultInFixedRange) {
  constexpr std::size_t b = 8;
  static const int MaxVal = static_cast<int>(std::pow(10.0, 9.0));
  mt19937 Generator;
  uniform_int_distribution<> Dist(1, std::numeric_limits<int>::max());
  SourceAccumulator Accum(b);

  for (int i = 0; i < MaxVal; i++) {
    int Obs = Dist(Generator);
    Accum.addObservation(Obs);
  }

  compareStdErr(0.02, MaxVal, Accum());
}

TEST(HLLTest, mergedAccumulatorsShowSameResults) {
  constexpr std::size_t b = 8;
  static const int MaxVal = static_cast<int>(std::pow(10.0, 9.0));
  mt19937 Generator;
  uniform_int_distribution<> Dist(1, std::numeric_limits<int>::max());
  SourceAccumulator SingleAccum(b);
  constexpr std::size_t NumAccumulators = 256;
  std::vector<SourceAccumulator> MergingAccumulators(NumAccumulators, b);
  auto CurrMergingAccum = MergingAccumulators.begin();

  for (int i = 0; i < MaxVal; i++) {
    int Obs = Dist(Generator);
    // Add to the single acuumulator
    SingleAccum.addObservation(Obs);
    // Add the observation to a the next accumulator in the merge list
    (*CurrMergingAccum).addObservation(Obs);
    if (++CurrMergingAccum == MergingAccumulators.end()) {
      CurrMergingAccum = MergingAccumulators.begin();
    }
  }

  // Collapse all of the accumulators in the merge list
  CurrMergingAccum = MergingAccumulators.begin();
  SourceAccumulator &FirstAccumulator = *CurrMergingAccum;
  ++CurrMergingAccum;
  for (; CurrMergingAccum != MergingAccumulators.end(); ++CurrMergingAccum) {
    FirstAccumulator.mergeIn((*CurrMergingAccum).bucketsBegin(),
                             (*CurrMergingAccum).bucketsEnd());
  }

  ASSERT_EQ(SingleAccum(), FirstAccumulator());
}
