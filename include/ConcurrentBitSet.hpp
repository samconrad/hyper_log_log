#ifndef CONCURRENT_BITSET_H
#define CONCURRENT_BITSET_H

#include "BitUtils.hpp"

#include <algorithm>
#include <atomic>
#include <climits>
#include <cstdint>
#include <iterator>
#include <limits>
#include <numeric>
#include <stdexcept>
#include <tuple>

template <typename BT> class BitReference;
template <typename BT, bool IsConst> class BitIterator;

template <std::size_t Size, std::size_t NumWords> class ConcurrentBitSetBase {
protected:
  friend class BitReference<ConcurrentBitSetBase>;
  friend class BitIterator<ConcurrentBitSetBase, true>;
  friend class BitIterator<ConcurrentBitSetBase, false>;

  using WordType = std::uint64_t;
  using WordEntry = std::atomic<WordType>;
  using OffsetInfo = std::pair<const std::size_t, const std::size_t>;

  using ReferenceType = BitReference<ConcurrentBitSetBase>;
  using ConstReferenceType = bool;
  using Iterator = BitIterator<ConcurrentBitSetBase, false>;
  using ConstIterator = BitIterator<ConcurrentBitSetBase, true>;
  using DiffType = std::ptrdiff_t;

private:
  using EntryIter = WordEntry *;

protected:
  constexpr ConcurrentBitSetBase();
  template <typename IntegralType, bool ReverseBits,
            typename = std::enable_if_t<std::is_integral<IntegralType>::value>>
  constexpr explicit ConcurrentBitSetBase(const IntegralType InitValue);

  constexpr std::size_t size() noexcept;
  std::size_t count(std::memory_order MemOrd) const noexcept;
  bool test(std::size_t, std::memory_order MemOrd) const;
  void set(std::memory_order MemOrd) noexcept;
  void set(std::size_t Pos, bool Val, std::memory_order MemOrd);
  ReferenceType makeRef(std::size_t Pos, std::memory_order LoadOrd,
                        std::memory_order StoreOrd) noexcept;
  ConstReferenceType makeRef(std::size_t Pos, std::memory_order LoadOrd) const
      noexcept;
  Iterator makeBeginIter(std::memory_order ImplicitLoadOrder,
                         std::memory_order ImplicitStoreOrder) noexcept;
  // TODO: obviously const begin iter with store order makes no sense, make this more clear
  Iterator makeBeginIter(std::memory_order ImplicitLoadOrder,
                         std::memory_order ImplicitStoreOrder) const noexcept;
  Iterator makeEndIter() const noexcept;

  static constexpr std::size_t BitsInWord =
      std::numeric_limits<WordType>::digits;

private:
  EntryIter beginEntries() noexcept;
  EntryIter endEntries() noexcept;
  static constexpr std::size_t computeWordCount(std::size_t);
  static constexpr OffsetInfo getWordOffsetInfo(std::size_t Pos);
  static bool loadBitValue(const WordEntry &Entry, const std::size_t Offset,
                           std::memory_order MemOrd) noexcept;
  static bool storeBitValue(WordEntry &Entry, const std::size_t Offset,
                            bool Value, std::memory_order MemOrd) noexcept;
  static bool flipBitValue(WordEntry &Entry, const std::size_t Offset,
                           std::memory_order MemOrd) noexcept;

  WordEntry WordSet[NumWords];
};

template <std::size_t S, std::size_t NW>
constexpr ConcurrentBitSetBase<S, NW>::ConcurrentBitSetBase() {
  std::fill(beginEntries(), endEntries(), WordType(0));
}

template <std::size_t S, std::size_t NW>
template <typename IntegralType, bool ReverseBits, typename Ignore>
constexpr ConcurrentBitSetBase<S, NW>::ConcurrentBitSetBase(
    const IntegralType InitValue) {
  if (ReverseBits) {
    InitValue = BitTables::reverseBits(InitValue);
  }
  constexpr std::size_t NumShifts = sizeof(IntegralType) / sizeof(WordType);
  constexpr std::size_t RemShift =
      BitsInWord - (sizeof(IntegralType) % sizeof(WordType)) * CHAR_BIT;
  // Shift the leftmost blocks
  EntryIter EntryIt = beginEntries(), EndIt = endEntries();
  for (std::size_t i = 1; i <= NumShifts && EntryIt != EndIt; ++i, ++EntryIt) {
    const std::size_t ShiftDist =
        (sizeof(IntegralType) - sizeof(WordType) * i) * CHAR_BIT;
    *EntryIt = static_cast<WordType>(InitValue >> ShiftDist);
  }
  // Shift the remainder
  if (EntryIt != EndIt) {
    *EntryIt = static_cast<WordType>(InitValue << RemShift);
    ++EntryIt;
  }
  std::fill(EntryIt, EndIt, WordType(0));
}

// protected

template <std::size_t S, std::size_t NW>
constexpr std::size_t ConcurrentBitSetBase<S, NW>::size() noexcept {
  return S;
}

template <std::size_t S, std::size_t NW>
std::size_t ConcurrentBitSetBase<S, NW>::count(std::memory_order MemOrd) const
    noexcept {
  return std::accumulate(beginEntries(), endEntries(), std::size_t(0),
                         [MemOrd](std::size_t Accum, WordEntry &Entry) {
                           return Accum +
                                  BitTables::countOnes(Entry.load(MemOrd));
                         });
}

template <std::size_t S, std::size_t NW>
bool ConcurrentBitSetBase<S, NW>::test(std::size_t Pos,
                                       std::memory_order MemOrd) const {
  if (Pos >= S) {
    throw std::out_of_range("Position " + std::to_string(Pos) +
                            " is too large for set size " + std::to_string(S));
  }
  return makeRef(Pos, MemOrd);
}

template <std::size_t S, std::size_t NW>
void ConcurrentBitSetBase<S, NW>::set(std::size_t Pos, bool Val,
                                      std::memory_order MemOrd) {
  std::size_t WordOffset, BitOffset;
  std::tie(WordOffset, BitOffset) = getWordOffsetInfo(Pos);
  storeBitValue(WordSet[WordOffset], BitOffset, Val, MemOrd);
}

template <std::size_t S, std::size_t NW>
typename ConcurrentBitSetBase<S, NW>::ReferenceType
ConcurrentBitSetBase<S, NW>::makeRef(std::size_t Pos, std::memory_order LoadOrd,
                                     std::memory_order StoreOrd) noexcept {
  std::size_t WordOffset, BitOffset;
  std::tie(WordOffset, BitOffset) = getWordOffsetInfo(Pos);
  return {WordSet[WordOffset], BitOffset, LoadOrd, StoreOrd};
}

template <std::size_t S, std::size_t NW>
bool ConcurrentBitSetBase<S, NW>::makeRef(std::size_t Pos,
                                          std::memory_order MemOrd) const
    noexcept {
  std::size_t WordOffset, BitOffset;
  std::tie(WordOffset, BitOffset) = getWordOffsetInfo(Pos);
  return loadBitValue(WordSet[WordOffset], BitOffset, MemOrd);
}

template <std::size_t S, std::size_t NW>
bool ConcurrentBitSetBase<S, NW>::loadBitValue(
    const WordEntry &Entry, const std::size_t Offset,
    std::memory_order MemOrd) noexcept {
  const WordType WordVal = Entry.load(MemOrd);
  return (WordVal >> Offset) & 0x1;
}

template <std::size_t S, std::size_t NW>
bool ConcurrentBitSetBase<S, NW>::storeBitValue(
    WordEntry &Entry, const std::size_t Offset, bool Value,
    std::memory_order MemOrd) noexcept {
  WordType Previous;
  if (Value) {
    const WordType WordVal = WordType(1) << Offset;
    Previous = Entry.fetch_or(WordVal, MemOrd);
  } else {
    const WordType WordVal = ~(WordType(1) << Offset);
    Previous = Entry.fetch_and(WordVal, MemOrd);
  }
  return (Previous >> Offset) & 0x1;
}

template <std::size_t S, std::size_t NW>
bool ConcurrentBitSetBase<S, NW>::flipBitValue(
    WordEntry &Entry, const std::size_t Offset,
    std::memory_order MemOrd) noexcept {
  WordType Previous, ModifiedValue;
  do {
    Previous = Entry.load(std::memory_order_relaxed);
    ModifiedValue = Previous ^ (~Previous | (WordType(1) << Offset));
  } while (!Entry.compare_exchange_weak(Previous, ModifiedValue, MemOrd,
                                        std::memory_order_relaxed));
  return (Previous >> Offset) & 0x1;
}

// private

template <std::size_t S, std::size_t NW>
typename ConcurrentBitSetBase<S, NW>::EntryIter
ConcurrentBitSetBase<S, NW>::beginEntries() noexcept {
  return WordSet;
}

template <std::size_t S, std::size_t NW>
typename ConcurrentBitSetBase<S, NW>::EntryIter
ConcurrentBitSetBase<S, NW>::endEntries() noexcept {
  return WordSet + NW;
}

template <std::size_t S, std::size_t NW>
constexpr std::size_t
ConcurrentBitSetBase<S, NW>::computeWordCount(std::size_t NumBits) {
  return (NumBits + BitsInWord - 1) / BitsInWord;
}

template <std::size_t S, std::size_t NW>
constexpr typename ConcurrentBitSetBase<S, NW>::OffsetInfo
ConcurrentBitSetBase<S, NW>::getWordOffsetInfo(std::size_t Pos) {
  const std::size_t WordPos = Pos / BitsInWord;
  // Left offset from the least significant digit
  const std::size_t BitOffsetInWord = BitsInWord - (Pos % BitsInWord);
  return {WordPos, BitOffsetInWord};
}

template <typename BT, bool IsConst> class BitIterator;

template <typename BitSet> class BitReference final {
public:
  BitReference() = delete;

  BitReference &operator=(const BitReference &Other) noexcept {
    // TODO: does this also need a store?  look at std::bitset behavior
    Entry = Other.Entry;
    Offset = Other.Offset;
    return *this;
  }

  BitReference &operator=(const bool Value) noexcept {
    return store(Value, ISO);
  }

  operator bool() const noexcept { return load(ILO); }

  bool operator~() const noexcept { return !load(ILO); }

  bool load(std::memory_order MemOrd) const noexcept {
    return BitSet::loadBitValue(Entry, Offset, MemOrd);
  }

  BitReference &store(bool Value, std::memory_order MemOrd) noexcept {
    BitSet::storeBitValue(Entry, Offset, Value, MemOrd);
    return *this;
  }

  BitReference &flip(std::memory_order MemOrd) noexcept {
    BitSet::flipBitValue(Entry, Offset, MemOrd);
    return *this;
  }

  BitReference &flip() noexcept { return flip(ISO); }

private:
  friend class BitIterator<BitSet, false>;

  BitReference(typename BitSet::WordEntry &Entry, const std::size_t Offset,
               std::memory_order ImplicitLoadOrder,
               std::memory_order ImplicitStoreOrder)
      : Entry(Entry), Offset(Offset), ILO(ImplicitLoadOrder),
        ISO(ImplicitStoreOrder) {}

  typename BitSet::WordEntry &Entry;
  std::size_t Offset;
  std::memory_order ILO;
  std::memory_order ISO;
};

template <typename BT, bool IsConst> struct MakeRef {};

template <typename BT> struct MakeRef<BT, true> {
  using ReferenceType = bool;
  static ReferenceType makeRef(typename BT::WordEntry &Entry,
                               const std::size_t Offset,
                               std::memory_order LoadOrder,
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Wunused-parameter"
                               std::memory_order StoreOrder) {
#pragma clang diagnostic pop
    return BT::loadBitValue(Entry, Offset, LoadOrder);
  }
};

template <typename BT> struct MakeRef<BT, false> {
  using ReferenceType = BitReference<BT>;
  static ReferenceType makeRef(typename BT::WordEntry &Entry,
                               const std::size_t Offset,
                               std::memory_order LoadOrder,
                               std::memory_order StoreOrder) {
    return BitReference<BT>(Entry, Offset, LoadOrder, StoreOrder);
  }
};

template <typename BT, bool IsConst> class BitIterator final {
private:
  using EntryIter = typename BT::EntryIter;
  using EntryType = typename std::remove_pointer<EntryIter>::type;
  static constexpr std::size_t BitsInWord = BT::BitsInWord;

public:
  using difference_type = typename BT::DiffType;
  using value_type = bool;
  using pointer = BitIterator;
  using reference = typename MakeRef<BT, IsConst>::ReferenceType;
  using iterator_category = std::random_access_iterator_tag;

  BitIterator(EntryIter Entry, std::size_t Offset,
              std::memory_order ImplicitStoreOrder,
              std::memory_order ImplicitLoadOrder)
      : CurrEntry(Entry), CurrOffset(Offset), ILO(ImplicitLoadOrder),
        ISO(ImplicitStoreOrder) {}

  BitIterator &operator++() noexcept {
    if (CurrOffset < BitsInWord - 1)
      ++CurrOffset;
    else {
      CurrOffset = 0;
      ++CurrEntry;
    }
  }

  BitIterator &operator++(int)noexcept {
    BitIterator Prev = *this;
    ++(*this);
    return Prev;
  }

  BitIterator &operator--() noexcept {
    if (CurrOffset > 0)
      --CurrOffset;
    else {
      CurrOffset = BitsInWord - 1;
      --CurrEntry;
    }
  }

  BitIterator &operator--(int)noexcept {
    BitIterator Prev = *this;
    --(*this);
    return Prev;
  }

  BitIterator &operator+=(difference_type Diff) noexcept {
    CurrEntry += (Diff >= 0 ? (Diff + CurrOffset)
                            : (static_cast<difference_type>(CurrOffset) + Diff -
                               BitsInWord + 1)) /
                 static_cast<difference_type>(BitsInWord);
    CurrOffset = static_cast<std::size_t>(
        (static_cast<difference_type>(CurrOffset) + Diff) % BitsInWord);
    return *this;
  }

  BitIterator &operator-=(difference_type Diff) noexcept {
    return *this += -Diff;
  }

  // return new iterators that preserve ILO and ISO

  BitIterator operator+(difference_type Diff) const noexcept {
    BitIterator Other(*this);
    Other += Diff;
    return Other;
  }

  BitIterator operator-(difference_type Diff) const noexcept {
    BitIterator Other(*this);
    Other -= Diff;
    return Other;
  }

  friend difference_type operator-(BitIterator &It1,
                                   BitIterator &It2) noexcept {
    return (It1.CurrEntry - It2.CurrEntry) * BitsInWord + It1.CurrOffset -
           It2.CurrOffset;
  }

  // comparison operators do not take ISO and ILO into account

  friend bool operator==(const BitIterator &It1,
                         const BitIterator &It2) noexcept {
    return It1.CurrEntry == It2.CurrEntry && It1.CurrOffset = It2.CurrOffset;
  }

  friend bool operator!=(const BitIterator &It1,
                         const BitIterator &It2) noexcept {
    return !(It1 = It2);
  }

  friend bool operator<(const BitIterator &It1,
                        const BitIterator &It2) noexcept {
    return It1.CurrEntry < It2.CurrEntry ||
           (It1.CurrEntry == It2.CurrEntry && It1.CurrOffset < It2.CurrOffset);
  }

  friend bool operator>(const BitIterator &It1, const BitIterator &It2) {
    return It1.CurrEntry > It2.CurrEntry ||
           (It1.CurrEntry == It2.CurrEntry && It1.CurrOffset > It2.CurrOffset);
  }

  friend bool operator<=(const BitIterator &It1,
                         const BitIterator &It2) noexcept {
    return !(It1 > It2);
  }

  friend bool operator>=(const BitIterator &It1,
                         const BitIterator &It2) noexcept {
    return !(It1 < It2);
  }

  reference operator[](difference_type Offset) const noexcept {
    return *(*this + Offset);
  }

  reference operator*() noexcept {
    return MakeRef<BT, IsConst>::makeRef(CurrEntry, CurrOffset, ILO, ISO);
  }

private:
  EntryIter CurrEntry;
  std::size_t CurrOffset;
  std::memory_order ILO;
  std::memory_order ISO;
};

// TODO:
// REORGANIZE
// Map out how inheritance hierarchy/private inheritance will work
// (multiple inheritance of iterator base bit set base?)
// Copy constructors, check if they will be implicitly generated
// actual ConcurrentBitSet class
// ADD TESTS AS ConcurrentBitSet is built
// all(), any(), etc
// operators (|=, ^=, <<=, >>= to ==, <, >, etc
// Note that some of these operators return a new bitset, need to make bitset
// copy constructor correctly
//(cc should use seq_cst)
//
// Certain operations are linearizable (ie |=), some are not (ie ^=)

#endif // CONCURRENT_BITSET_H
