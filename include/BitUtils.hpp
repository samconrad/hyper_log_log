#include <array>
#include <cstdint>

namespace BitTables {
namespace {

inline constexpr std::size_t ByteMul() { return 8; }

template <typename T>
using IsIntegral = std::enable_if_t<std::is_integral<T>::value>;

template <typename Out> using FuncType = Out (*)(std::uint8_t);

template <typename Out, FuncType<Out> F, std::uint8_t Byte, Out... Outputs>
struct Lookup : Lookup<Out, F, Byte - 1, F(Byte), Outputs...> {};

template <typename Out, FuncType<Out> F, Out... Outputs>
struct Lookup<Out, F, 0, Outputs...> {
  static constexpr std::array<Out, sizeof...(Outputs) + 1> Table{F(0),
                                                                 Outputs...};
};

// VS doesn't seem to be up to speed yet on constexpr loops so this is done
// recursively instead
inline constexpr std::size_t countLeadingZeroes(std::uint8_t Val, int Idx,
                                                std::size_t Accum) {
  return Idx >= 0 && !((Val >> Idx) & 0x1)
             ? countLeadingZeroes(Val, Idx - 1, Accum + 1)
             : Accum;
}

inline constexpr std::size_t countLeadingZeroes(std::uint8_t Val) {
  return countLeadingZeroes(Val, ByteMul() - 1, 0);
}

inline constexpr std::size_t countOnes(std::uint8_t Val, int Idx,
                                       std::size_t Accum) {
  return Idx >= 0 ? countOnes(Val, Idx - 1, Accum + ((Val >> Idx) & 0x1))
                  : Accum;
}

inline constexpr std::size_t countOnes(std::uint8_t Val) {
  return countOnes(Val, ByteMul() - 1, 0);
}

inline constexpr std::uint8_t reverse(std::uint8_t Val, std::size_t Idx,
                                      std::uint8_t Accum) {
  return Idx < ByteMul()
             ? reverse(Val, Idx + 1,
                       Accum | ((Val >> (ByteMul() - 1 - Idx) & 0x1) << Idx))
             : Accum;
}

inline constexpr std::uint8_t reverse(std::uint8_t Val) {
  return reverse(Val, 0, std::uint8_t(0));
}
} // end anonymous namespace

template <typename T, typename = IsIntegral<T>>
inline std::size_t countLeadingZeroes(T Value) noexcept {
  constexpr auto Table =
      Lookup<std::size_t, &countLeadingZeroes, UINT8_MAX>::Table;
  std::size_t Counter = 0;
  for (std::size_t i = sizeof(Value); i > 0; i--) {
    const std::uint8_t Byte =
        static_cast<std::uint8_t>(Value >> ((i - 1) * ByteMul()));
    if (0 == Byte) {
      Counter += 8;
    } else {
      Counter += Table[static_cast<std::size_t>(Byte)];
      break;
    }
  }
  return Counter;
}

template <typename T, typename = IsIntegral<T>>
inline std::size_t countOnes(T Value) noexcept {
  constexpr auto Table = Lookup<std::size_t, &countOnes, UINT8_MAX>::Table;
  std::size_t Counter = 0;
  for (std::size_t i = sizeof(Value); i > 0; i--) {
    const std::uint8_t Byte =
        static_cast<std::uint8_t>(Value >> ((i - 1) * ByteMul()));
    Counter += Table[static_cast<std::size_t>(Byte)];
  }
  return Counter;
}

template <typename T, typename = IsIntegral<T>> inline T reverseBits(T Value) {
  constexpr auto Table = Lookup<std::uint8_t, &reverse, UINT8_MAX>::Table;
  T Reversed = 0;
  for (std::size_t i = 0; i < sizeof(Value); i++) {
    const std::uint8_t Byte =
        static_cast<std::uint8_t>(Value >> (i * ByteMul()));
    Reversed |= Table[static_cast<std::size_t>(Byte)];
    if (i < sizeof(Value) - 1) {
      Reversed <<= ByteMul();
    }
  }
  return Reversed;
}
} // end namespace BitTables
