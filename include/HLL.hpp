#ifndef HYPER_LOG_LOG_H
#define HYPER_LOG_LOG_H

#include "BitUtils.hpp"

#include <algorithm>
#include <array>
#include <cassert>
#include <cmath>
#include <cstdint>
#include <iterator>
#include <type_traits>

template <typename T, typename Hash = std::hash<T>> class Accumulator {
public:
  Accumulator(const std::size_t b, Hash &&H = Hash())
      : HashFunction(std::forward<Hash>(H)), Buckets(std::size_t(1) << b),
        BucketMask(buildMask(b)), Alpha(alpha(Buckets.size())) {}

  void addObservation(const T &Value) {
    const HashType HashVal = HashFunction(Value);
    const std::size_t BucketIdx = HashVal & BucketMask;
    const std::size_t LeadingZeroes =
        BitTables::countLeadingZeroes(HashVal | BucketMask);
    Buckets[BucketIdx] = std::max(Buckets[BucketIdx], LeadingZeroes);
  };

  auto bucketsBegin() { return Buckets.begin(); }

  auto bucketsEnd() { return Buckets.end(); }

  template <typename ForwardIterator,
            typename = std::enable_if_t<std::is_same<
                std::size_t, typename std::iterator_traits<
                                 ForwardIterator>::value_type>::value>>
  void mergeIn(ForwardIterator Begin, ForwardIterator End) {
    using DiffType =
        typename std::iterator_traits<ForwardIterator>::difference_type;
    const DiffType Size = std::distance(Begin, End);
    if (Size != static_cast<DiffType>(Buckets.size())) {
      throw std::invalid_argument("Input iterator had distance " +
                                  std::to_string(Size) + ", expected size " +
                                  std::to_string(Buckets.size()));
    }
    for (auto OtherIt = Begin, ThisIt = Buckets.begin(); OtherIt != End;
         ThisIt++, OtherIt++) {
      *ThisIt = std::max(*ThisIt, *OtherIt);
    }
  }

  std::size_t operator()() {
    const double m = Buckets.size();
    double Z = 0.0;
    for (const std::size_t Count : Buckets) {
      Z += 1.0 / std::pow(2.0, Count);
    }

    double E = m * m * (1.0 / Z) * Alpha;

    constexpr double Const_2_32 = std::pow(2.0, 32.0);
    constexpr double SmallRange = 5.0 / 2.0;
    constexpr double LargeRange = Const_2_32 / 30.0;
    if (E <= SmallRange) {
      const std::size_t V = std::count(Buckets.begin(), Buckets.end(), 0);
      if (V > 0) {
        E = m * std::log(m / V);
      }
    } else if (E > LargeRange) {
      E = -Const_2_32 * std::log(1 - E / Const_2_32);
    }
    return static_cast<std::size_t>(E);
  }

private:
  using HashType = decltype(std::declval<Hash>()(std::declval<T>()));

  static double alpha(const std::size_t M) {
    switch (M) {
    case 16u:
      return 0.673;
    case 32u:
      return 0.697;
    case 64u:
      return 0.709;
    default:
      return 0.7213 / (1.0 + 1.079 / M);
    }
  }

  static HashType buildMask(std::size_t NumBits) {
    // Make sure we'll have at least some bits left over for the actual
    // estimation
    if (sizeof(HashType) * 8 <= NumBits) {
      throw std::invalid_argument(
          "Number of mask bits is too large for hash type");
    }
    return (HashType(1) << NumBits) - 1;
  }

  Hash HashFunction;
  std::vector<std::size_t> Buckets;
  const HashType BucketMask;
  const double Alpha;
};

template <typename ForwardIterator,
          typename ValueType =
              typename std::iterator_traits<ForwardIterator>::value_type,
          typename Hash = std::hash<ValueType>>
inline std::size_t hll_estimate(ForwardIterator Begin, ForwardIterator End,
                                std::size_t b, Hash &&H = Hash()) {
  Accumulator<ValueType, Hash> Accum(b, std::forward<Hash>(H));

  for (auto It = Begin; It != End; ++It) {
    Accum.addObservation(*It);
  }

  return Accum();
}

#endif //  HYPER_LOG_LOG_H
